\documentclass[conference]{IEEEtran}
%\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{xcolor}
\usepackage{listings}
\usepackage{float}
\usepackage{url}
\usepackage[edges]{forest}

\lstset{basicstyle=\ttfamily,
  showstringspaces=false,
  commentstyle=\color{red},
  keywordstyle=\color{blue}
}

\definecolor{foldercolor}{RGB}{124,166,198}

\tikzset{pics/folder/.style={code={%
    \node[inner sep=0pt, minimum size=#1](-foldericon){};
    \node[folder style, inner sep=0pt, minimum width=0.3*#1, minimum height=0.6*#1, above right, xshift=0.05*#1] at (-foldericon.west){};
    \node[folder style, inner sep=0pt, minimum size=#1] at (-foldericon.center){};}
    },
    pics/folder/.default={10pt},
    folder style/.style={draw=foldercolor!80!black,top color=foldercolor!40,bottom color=foldercolor}
}

\forestset{is file/.style={edge path'/.expanded={%
        ([xshift=\forestregister{folder indent}]!u.parent anchor) |- (.child anchor)},
        inner sep=1pt},
    this folder size/.style={edge path'/.expanded={%
        ([xshift=\forestregister{folder indent}]!u.parent anchor) |- (.child anchor) pic[solid]{folder=#1}}, inner xsep=0.6*#1},
    folder tree indent/.style={before computing xy={l=#1}},
    folder icons/.style={folder, this folder size=#1, folder tree indent=2.5*#1},
    folder icons/.default={10pt},
}

\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{pysimCoder -- Open-Source Rapid Control Prototyping for GNU/Linux and NuttX}

\author{\IEEEauthorblockN{1\textsuperscript{st} Michal Lenc}
\IEEEauthorblockA{\textit{Department of Control Engineering} \\
\textit{CTU FEE}\\
Prague, Czech Republic \\
lencmich@fel.cvut.cz}
\and
\IEEEauthorblockN{2\textsuperscript{nd} Pavel Píša}
\IEEEauthorblockA{\textit{Department of Control Engineering} \\
\textit{CTU FEE}\\
Prague, Czech Republic \\
pisa@fel.cvut.cz}
\and
\IEEEauthorblockN{3\textsuperscript{nd} Roberto Bucher}
\IEEEauthorblockA{\textit{Department of Innovative Technologies} \\
\textit{SUPSI}\\
Lugano, Switzerland \\
roberto.bucher@supsi.ch}
}

\maketitle

\begin{abstract}
This paper introduces an open source alternative for design of real time control systems for affordable hardware. It presents
the combination of pysimCoder, a rapid control prototyping tool, POSIX compliant operating systems NuttX or GNU/Linux and
Silicon Heaven infrastructure used for runtime monitoring and tuning of model parameters.

The capability of used software is demonstrated on a real time control of a permanent magnet synchronous motor with
imxRT microcontroller based board Teensy 4.1.
\end{abstract}

\begin{IEEEkeywords}
rapid control prototyping, real time control, RTOS, PMSM control
\end{IEEEkeywords}

\section{Introduction}
There are currently many systems available for the design of real time control applications. While proprietary tools like Matlab/Simulink
are widely used in the industry for their stability and amount of offered functions, open source alternatives may be preferable for educational
purposes. There are several reasons for this. For example the possibility to explore how the code is written and add own functions to it. The
economical aspect is also a huge benefit of open source applications.

This paper introduces the combination of pysimCoder, a control application development tool with block editor and code generator, and Silicon
Heaven infrastructure (SHV) used for runtime monitoring and tuning of model parameters. PysimCoder currently supports two POSIX compliant open source
real time operating systems: GNU/Linux and NuttX. The latter offers a wide support for smaller and cheaper microcontrollers and boards like imxRT series from NXP company.
The design of permanent magnet synchronous motor control with pysimCoder, NuttX and imxRT 1060 MCU is used as a demonstration of pysimCoder and Silicon
Heaven control capabilities.

\section{pysimCoder}
PysimCoder is an open source rapid control application development tool designed by Professor Roberto Bucher from the University of Applied Sciences and
Arts of Southern Switzerland\cite{b1}.

It uses an extended python-control library for the integration of methods as full or reduced state space observer, anti-windup
mechanism and discrete linear quadratic regulator\cite{b2}. Apart from that pysimCoder offers a graphical editor with a code generator. This combination allows the user
to design the desired application graphically by using predefined blocks and then generate a C code that can be run on a target hardware. The example of pysimCoder's block
diagram application can be seen in Fig.~\ref{pysim-window}.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.5\textwidth]{figs/pysim-window.png}}
\caption{PysimCoder's block editor\cite{b3}.}
\label{pysim-window}
\end{figure}

The left part of the Fig.~\ref{pysim-window} shows the library of available blocks. Each block has its own parameters that can be set by the user during the design
(e.g. gain constants of PID controller or PWM frequency). Drag and drop method is used to place the blocks to the block diagram window\cite{b1}. The parameter
setting is opened by double left click on the block placed in a diagram window. Some blocks also support multiple inputs/outputs. This selection can be accessed via
single right click on the block\cite{b4}.

\subsection{Code Generation}
Two equations, \eqref{ss-block-x} and \eqref{ss-block-y} representing internal states and outputs, respectively, are used for the description of each
block in pysimCoder.

\begin{equation}
x_{k+1}=f(x_k, u_k, k)\label{ss-block-x},
\end{equation}
\begin{equation}
y_{k}=g(x_k, u_k, k)\label{ss-block-y}.
\end{equation}

These two equations are executed in an opposite order in a generated application. Therefore system firstly updates block’s output,
described by equation \eqref{ss-block-y} and then internal state per equation \eqref{ss-block-x}. This ensures the blocks do not need to remember their previous
state as it would be required if equation \eqref{ss-block-x} would be executed before equation \eqref{ss-block-y}.

Apart from those two functions, an initialization and a termination function is also provided for each block. These functions are used for device driver initialization and
termination for example. The code generation process takes a designed block diagram and an optional Python script (used for implementation of the controller
and variables definitions) and generates a Python script tmp.py. This script is used for translation of designed block diagram into C code that ensures the blocks
are initialized during program start up and their specific functions are then called at each control period. The call of specific function has to be done in correct
order based on blocks' dependencies on each other. Generation process needs to take care of this and find the right execution sequence\cite{b4}.
The generation process is shown in Fig.~\ref{codegen-diag}\cite{b4}.

The executable main function is generated from target operating system specific template. Compilation is controlled by Makefile generated from the second target system
specific template.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.5\textwidth]{figs/codegen-diag.pdf}}
\caption{PysimCoder Code Generation Process.}
\label{codegen-diag}
\end{figure}

The generated C code is subsequently compiled and linked with the block library and main C file with the real time thread and creates an executable\cite{b4}.

Since with this approach the block diagram is translated into a single C module, with all the various library function calls, the generated code can also be easily ported
to other environments. For example a STM32 Cube with FreeRTOS kernel\cite{b5}.

\subsection{Block Definition in Library}

PysimCoder has two blocks related folders in its structure, \textit{resources} and \textit{CodeGen}. The first mentioned folder provides blocks'
declaration in JSON format and Python part of the code while the latter contains Makefile templates and C parts of blocks' code for supported targets.

The first declaration of the block is provided in \textit{resources} directory. It contains \textit{.xblk} files in JSON format that provides declaration
of block's inputs, outputs, parameters and other required instances. The file also provides the link to block's Python function also located in \textit{resources}
directory. The function provides the name of the block's C function used for runtime execution and passes block's parameters to the structure used from C code layer.

C files for the blocks are located in \textit{CodeGen} directory. These files contains functions that take care of blocks' execution during runtime. Therefore block
initialization, output update, internal state update or termination can be found in this directory.

\section{Runtime Monitoring and Tuning of Model Parameters}

The goal of runtime monitoring and tuning of model parameters is to allow the user to display and edit parameters, inputs and outputs of individual blocks.
Silicon Heaven infrastructure (also abbreviated as SHV) developed at Czech company Elektroline a.s was used to achieve this goal \cite{shv}.
The infrastructure has been conceived in the company to provide communication backbone for their tramway yards control solutions in Australia, Belgium and
other countries around the globe\cite{b4}.

An open remote procedure call protocol (RPC) called ChainPack is implemented by SHV for data serialization. The serialization format combines the properties of
Extensible Markup Language (XML) and JavaScript Object Notation (JSON). Support for many programming languages is provided, including C, C++, Python, pure
JavaScript or Rust. The infrastructure requires running a broker as a server while user applications as pysimCoder control application or GUI designed to
interact with the broker are registered to the broker as clients. Each client can have different rights and setting based on the broker’s configuration\cite{b4}.

In addition to its main library, libshv, which implements ChainPack RCP, the infrastructure provides the GUI tool shvspy that can be used to interact with the broker
(e.g. change model's parameters). Source tree shvapp is also included and implements shvbroker to which client applications can be connected.

\subsection{ChainPack RPC}

Every ChainPack RPC message consists of three fields: length, protocol and data, respectively. The length field stores an unsigned integer representing length of the
message without the length field itself, therefore the length of protocol field plus the length of data field. Protocol is once again an unsigned integer defining data
format (ChainPack RPC, Cpon or JSON).

\subsection{Supported Methods}

Clients in Silicon Heaven hiearchy store their items (model's parameters for example) and comunicate through common ancestor broker via defined methods.
According to SHV standard, there are two mandatory methods that has to be supported by every item: \textit{ls} and \textit{dir}. The list of strings with the
names of node's children (empty when item has no children) is returned by the  first method. Method \textit{dir} returns the list of
methods supported by the node. In any case those are two already mentioned methods plus item specific methods.

Every block's parameter's node also supports methods \textit{get}, \textit{set} and \textit{typeName}. As the names already suggest, first two methods send node actual
and applay received value. Furthemore \textit{typeName} returns the type of the value. Currently only double values are supported for model's introspection as pysimCoder
sometimes uses integer values for block's inner state representation and those values should not be propagated and editable from the final tree\cite{b4}.

\section{Target System: NuttX}

PysimCoder provides support for two POSIX compliant operating system, GNU/Linux and NuttX. This section describes the latter operating system which is also used in the
following PMSM control example.

NuttX is a real time operating systems initially introduced by Gregory Nutt in 2007 and currently managed by The Apache Software Foundation. It is provided
as an open source project under Apache License 2.0 lincension. The operating system is written in C language and uses Kconfig system inspired by GNU/Linux kernel.
The POSIX compatibility means the applications written in source code level compliance with POSIX standards (on GNU/Linux for example) can be compiled and run on
NuttX as well with minimal or possibly even no changes. This allows the pysimCoder to use the same generic blocks (UDP, TCP/IP blocks or timing mechanisms for
example) for both GNU/Linux and NuttX and thus simplify the block editor's complexity.

NuttX primarily targets small and cheap microcontrollers and boards. This is thanks to linking from static libraries and using many source files that contain
only one or function or small number of functions if needed. This allows the build system to include only required functions as selected by the user
during the configuration. NuttX documentation mentions that the final executable can then be run on only 32 kB of total memory (code and data). However typical NuttX
build needs about at least 64 kB of memory\cite{b6}.

Most of the commonly used instruction set architectures (ISAs) are supported by NuttX. The most extended support is provided for ARM instruction set architecture (ISA)
nowadays used in most of the microcontrollers for embedded systems\cite{b7}. Therefore versions of STM32 chips, microcontrollers from imxRT series, LPC series
or SAM series are supported as well as other ARM based microcontrollers. Other architectures like Xtensa with support for microcontrollers designed by Espressif
company, widely known Intel x86 ISA or open standard architecture RISC-V are also supported but so far not as widely as ARM\cite{b4}\cite{dion}.

\subsection{NuttX Integration with pysimCoder}

PysimCoder offers several NuttX specific blocks to interact with the MCU's peripherals. This includes support for device drivers like ADC, PWM, encoder, CAN (including
SocketCAN) or serial port. Some blocks from communication library as TCP and UDP respect POSIX standard and they can be used with NuttX as well\cite{b4}.

\section{Example: Permanent Magnet Synchronous Motor Control}

This section covers the entire process of NuttX preparation, configuration and compilation, the design of the control application with pysimCoder and the connection of the
Silicon Heaven infrastructure on the example of controlling a permanent magnet synchronous motor.

The compilation steps are documented for Linux distribution Ubuntu (version 20.04 and newer) but this should be the same for other Debian based Linux distributions.
The compilation requires the kconfig-frontend package to be installed on the system. Architecture specific toolchain (e.g. arm-none-eabi-gcc for ARM targets) is
also required\cite{b4}.

\subsection{NuttX Configuration}

The first step of real time control application design is the configuration and compilation of NuttX real time operating system. This example uses mainline GitHub version
of RTOS but it is also possible to download a stable release from \cite{b8}.

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
git clone https://github.com/apache/\
  nuttx.git nuttx
git clone https://github.com/apache/\
  nuttx-apps.git apps
cd nuttx
./tools/configure.sh teensy-4.1:pikron-bb
make
\end{lstlisting}

Steps listed above are required to compile NuttX RTOS for imxRT MCU based Teensy 4.1 board used in this example. The pikron-bb stands for used predefined configuration
in NuttX mainline. It is also possible to edit the predefined configuration by executing the following command.

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
make menuconfig
\end{lstlisting}

Once NuttX is compiled, it is necessary to run command

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
make export
\end{lstlisting}

that generates \textit{nuttx-export-xx.x.x.tar.gz} with exported files allowing to build user application againts NuttX headers and link it with operating system
core libraries. \textit{xx.x.x} corresponds with the current NuttX version.

\subsection{Application Design with pysimCoder}

The previous subsection described the compilation and configuration of NuttX RTOS. The next step is a compilation of pysimCoder block library
for specific target and an application design. PysimCoder can be downloaded from GitHub repository by following command.

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
git clone https://github.com/\
  robertobucher/pysimCoder.git
\end{lstlisting}

Now it is required to copy the previously exported NuttX file \textit{nuttx-export-xx.x.x.tar.gz} to \textit{pysimCoder/CodeGen/nuttx} repository, unzip it and compile
block librearies. This can be done by using following commands.

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
cp nuttx-export-xx.x.x.tar.gz \
  ../pysimCoder/CodeGen/nuttx
cd /../pysimCoder/CodeGen/nuttx
tar -xvzf nuttx-export-xx.x.x.tar.gz
mv nuttx-export-xx.x.x nuttx-export
cd devices
make SHV=1
\end{lstlisting}

The directory \textit{devices} contains blocks' source files written in C language and defining the functions of the pysimCoder's blocks. Parameter
\textit{SHV=1} of the last command ensures Silicon-Heaven libraries are compiled with pysimCoder's files. PysimCoder can be compiled without these libraries if
the command is not used. This can be used if the runtime parameter access is not planned.

PysimCoder can similarly be compiled for GNU/Linux in directory \textit{pysimCoder/CodeGen/Linux} as well. Of course there is no need to export and unzip any files
for GNU/Linux.

In this stage all source code required for NuttX and pysimCoder integration is compiled and block editor can be opened. There are two possible approaches: pysimCoder
can either be installed on the computer as described in \cite{b1} or a bash script can be used to open pysimCoder directly from the source code. The script can be
run by executing the following command.

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
./pysim-run
\end{lstlisting}

This opens block editor and library described in section~2. Individual blocks can now be placed to the block diagram by drag and drop method and their parameters can
be changed by double left click on the block placed in a diagram window. Fig.~\ref{pmsm-pysim} shows the diagram which was designed to control PMSM possition according
to a reference signal. The incremental encoder provides an actual position for feedback control.

Apart from information about actual position, encoder input is also used to get a mechanical angle between permament magnets equipped rotor and stator with three phases
windings. However the angle can not be determined from the encoder until encoder reaches its index which can be a one rotation at maximum. For this reason three
Hall sensors are attached to the motor to approximate mechanical angle during the first revolution. This angle is not as precise as the calculation from the
encoder but it can be used until the encoder reaches its index. This logic is implemented in a block \textit{PMSM Align}.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.5\textwidth]{figs/pmsm-pysim.pdf}}
\caption{PMSM control block diagram in pysimCoder.}
\label{pmsm-pysim}
\end{figure}

\textit{Pulse Generator} block is used for the generation of reference positon. This reference is integrated which results in a triangle reference instead of square one.
Blocks for inverse Park and inverse Clarke transformation are used to convert D (set as 0), Q (driven by PID controller) and angle (calculated from Hall/encoder)
signals to three phased a, b, c vector that is used to set PWM duty cycle.

Current sensing with ADC peripheral would be requried for vector control (also called field oriented control and abbreviated as FOC) if this type of control would be
used. This step is pllaned for the following development phase as well as proting of the model predictive control (MPC) block developed for Matlab/Simulink and later
for plain C based controller in our previous projects.\cite{belda}

Silicon Heaven options can be selected in pysimCoder's menu under \textit{SHV} support icon. Two possible choices to prepresent SHV nodes organization, AVL tree based
GAVL and generic sorted array based object abbreviated as GSA, are available. Usage of GAVL is prefered when the tree is allocated dynamically during application start
as GSA uses more memory reallocation for addition of new items to the array. GSA however can be allocated statically during code generation and all items can be
constant and thus the whole can tree to be saved to flash memory\cite{b4}. The complete description of pysimCoder set up can be seen on the pysimCoder's
wiki page\cite{b12}.

The other important setting is to select a Template Makefile for the target, in this case \textit{nuttx.tmf}. This can be done in the top menu by clicking on
Block setting icon highlighted in the red circle in Fig.~\ref{pysim-menu}.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.5\textwidth]{figs/pysim-menu.png}}
\caption{PysimCoder Menu Option. Taken from \cite{b3}.}
\label{pysim-menu}
\end{figure}

The loadable executable can be generated by selecting Generate C-code icon highlighted in the green cirle in Fig.~\ref{pysim-menu}. The executable is a standard
NuttX system image with a terminal application run at startun. The included file system contains configured applications and control applications called \textit{main}.
This application realizes the designed control system. The complete manual describing application generation can be found in NuttX documentation linked in \cite{b3}.

\subsection{Usage of Silicon Heaven}

As mentioned in section~3, Silicon Heaven implements additional tools as shvspy and shvbroker. This section shows their compilation and usage with an application
generated in pysimCoder. Tool shvbroker acts as a TCP server throught which clients communicate with each other. Following commands can be used for the
compilation and usage.

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
git clone https://github.com/\
  silicon-heaven/shvapp.git
cd shvapp
git submodule update --init --recursive
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=.. ..
make
make install
cd ../bin
./shvbroker --config-dir \
  ../shvbroker/etc/shv/\
  shvbroker/ -v rpcmsg
\end{lstlisting}

In directory \textit{shvbroker/etc/shv/shvbroker} can be found configuration files that set up the broker. These include information like server port, host name,
password for users and so  on. These parameters have to be put in pysimCoder application in order to connect the application to the server.

Another used tool is called shvspy. This application is a graphical user interface that allows the user to interact with clients' items and call supported methods.
It is connected to the broker as well as pysimCoder application. The compilation and usage is described by the following commands.

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
git clone https://github.com/\
  silicon-heaven/shvspy
cd shvspy
git submodule update --init --recursive
mkdir build
cd build
cmake -DCMAKE_INSTALL_PREFIX=.. ..
make
make install
cd ../bin
LD_LIBRARY_PATH=../lib \
  ./shvspy -v rpcmsg
\end{lstlisting}

Once the application generated by pysimCoder is flashed into the target board and run, it automatically connects itself to the broker and can be accessed through
shvspy application.

\begin{figure}[htbp]
{\scriptsize
\begin{forest}
  for tree={font=\sffamily, grow'=0,
  folder indent=.9em, folder icons}
  [PMSM Control
    [blocks, this folder size=10pt
        [Discrete PID]
        [PulseGenerator
          [inputs]
          [outputs
            [output1, is file]
          ]
          [parameters
            [Amplitude, is file]
            [Bias, is file]
            [Delay, is file]
            [Period, is file]
            [Width, is file]
          ]
        ]
    ]
    [inputs]
    [outputs]
  ]
\end{forest}
}
\caption{Example of SHV tree structure.}
\label{shv-tree}
\end{figure}

Fig.~\ref{shv-tree} shows an example of the generated tree (only subset of the diagram is included for greater clarity). The application PMSM Control has
items \textit{blocks}, \textit{inputs} and \textit{outputs} where the last two groups are empty (contains no nodes) as no blocks specific for external signal
inputs and outputs are not used here. These signals can be used for the connection into distributed system setup. The first item however lists the
blocks used in the application as designed per Fig.~\ref{pmsm-pysim}.

Every block has accessible parameters, inputs and outputs. Inputs and outputs use generic naming \textit{input1} or \textit{output1}, parameters prefer to use the names
corresponding to the ones in pysimCoder. This however is not supported for each block so far therefore generic names as \textit{double1} might be used for some blocks.

While parameters are both readable and writable, inputs and outputs are read only as their purpose is model introspection and debugging. The parameters are editable via
the \textit{attributes} window shown in Fig.~\ref{shv-atributes}.

\begin{figure}[htbp]
\centerline{\includegraphics[width=0.5\textwidth]{figs/shv-atributes.png}}
\caption{Item's atributes in SHV tree structure.}
\label{shv-atributes}
\end{figure}

Example application uses a \textit{TCPsocketTXRX} block that sends a reference and actual position over TCP. These data can be subsequently visualised in a graph or saved
for futher analysis. PysimCoder provides a simple Python application for the visualization of received data.

There are multiple supported interfaces, including serial port, UDP or TCP. The application can be opened with following commands. Communication over TCP
is used for the example application.

\begin{lstlisting}[frame=single][language=bash,caption={bash version}]
cd BlockEditor/
python RTScope.py
\end{lstlisting}

The resulting history of reference (green) and actual (red) position can be seen in Fig.~\ref{pmsm-graph}.

\begin{figure}[H]
\centerline{\includegraphics[width=0.5\textwidth]{figs/pmsm-graph.png}}
\caption{Reference and actual position of PMSM control as seen in RT Scope.}
\label{pmsm-graph}
\end{figure}

The real time scope can be also used for debugging purposes, for example for the view of control action to PWM modules. This is once again an example of triangle
reference but the motor is braked by the hand for the latter half of the figure. The resulting control action, i.e. output of inverse Clark transformation and
therefore an input of PWM blocks, can be seen in Fig.~\ref{pwm-graph}.

\begin{figure}[H]
\centerline{\includegraphics[width=0.5\textwidth]{figs/pwm-graph.png}}
\caption{PWM control action as seen in RT Scope.}
\label{pwm-graph}
\end{figure}

\section{Conclusion}

The combination of pysimCoder can be an open source alternative to proprietary rapid control tool applications and can be used with an affordable hardware as demonstrated
in the example. Furthemore pysimCoder can be used for a system simulation. The open source status of pysimCoder allows relavively easy modification of current blocks
and the additional of potential new ones.

Added support of Silicon Heaven infrastructure allows the runtime monitoring and tuning of model parameters and the introspection of designed diagram. This brings the
possibility to experiment with a designed controller on a real hardware or debug the designed model.

There are still many parts of pysimCoder that provide space for improvements and new features. This is a case of getting correct parameters' names to pysimCoder's
generated C code. Generation of block's metadata during diagram creation would offer additional possibilites like deciding what parameters can be changed
runtime and what should be exposed as read only. Additon of integer parameters is also possible.

The repository with a designed block diagram and script that clones NuttX and pysimCoder repositories, configures and compiles NuttX and compiles pysimCoder can be
found in \cite{b9}. The PSMS control demo for imxRT platform is located in the directory \textit{platforms/imxrt/pmsm-control}.
Other pysimCoder related example are listed in \cite{b10}.

\begin{thebibliography}{00}
\bibitem{b1} Bucher, Roberto. Python for control purposes. Available from https://robertobucher.dti.supsi.ch/wp-content/uploads/2017/03/BookPythonForControl.pdf.
\bibitem{b2} Bucher, Roberto. Rapid Control Prototyping with pysimCoder and NuttX . Available from https://www.youtube.com/watch?v=y7NvFAp3OII.
\bibitem{b3} The Apache Foundation. pysimCoder integration with NuttX . Available from https://nuttx.apache.org/docs/latest/guides/pysimcoder.html.
\bibitem{b4} Lenc, Michal. Open Rapid Control Prototyping and Real-Time Systems, CTU Prague, 2022.
\bibitem{b5} \url{https://www.enib.fr/~kerhoas/pmsm_cours_index.html}.
\bibitem{shv} \url{https://github.com/silicon-heaven}
\bibitem{b6} The Apache Foundation. About Apache NuttX . Available from http://nuttx.incubator.apache.org/docs/latest/introduction/about.html.
\bibitem{b7} Güven, Yılmaz, Ercan Coşgun, Sıtkı Kocaoğlu, Harun Gezici, and Eray Yilmazlar. Understanding the Concept of Microcontroller Based Systems To Choose
The Best Hardware For Applications. Research Inventy: International Journal of Engineering And Science. 12, 2017, Vol. 6, No. 9, pp. 38-44. ISSN 278-4721.
\bibitem{dion} Begiri, Dion. Open Rapid Control Prototyping, Education and Design Tools, CTU Prague, 222.
\bibitem{b8} The Apache Foundation. Apache NuttX Downloads. Available from https://nuttx.apache.org/download/.
\bibitem{belda} Belda, K.; Píša, P. Explicit Model Predictive Control of PMSM Drives In: 2021 IEEE 30th International Symposium on Industrial Electronics (ISIE). Piscataway: IEEE Industrial Electronics Society, 2021. ISSN 2163-5145. ISBN 978-1-7281-9023-5. 
\bibitem{b12} \url{https://github.com/robertobucher/pysimCoder/wiki/Silicon-Heaven-Support}.
\bibitem{b9} NuttX demos repository including pysimCoder PSMS control https://gitlab.fel.cvut.cz/otrees/nuttx-demos, CTU FEE Open Technologies Research Education and Exchange Services, 2022.
\bibitem{b10} https://github.com/robertobucher/pysimCoder-examples.
\end{thebibliography}

\end{document}
